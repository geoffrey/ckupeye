# ckupeye

Small bash script for entire filesystem backups.

It backups filesystems, sure, but also creates workable version of
PostgreSQL / MariaDB backups, Pacman / Apt software list, ACL.

It then uses [BorgBackup](https://www.borgbackup.org/) to create, compress, encrypt and send the backups.
You'd probably want to be familiar with the latter if you want to understand this script fully.

**DISCLAIMER:** This program is just a publication of my personal script.
It haven't been tested outside of my (very) specific use case.
You'd probably better use it as a reference rather than as is.

## Usage

Basically it's just the `ckupeye` script.
Have some example installation instructions, for the client.

```bash
git clone https://git.frogeye.fr/geoffrey/ckupeye.git /opt/ckupeye
ln -s /opt/ckupeye/ckupeye /usr/local/bin/ckupeye
mkdir /etc/ckupeye
mkdir /var/lib/ckupeye
cp /opt/ckupeye/config.sample.sh /etc/ckupeye/config.sh
ssh-keygen -t ed25519 -C ckupeye@$(cat /etc/hostname) -f /etc/ckupeye/id_ed25519
$EDITOR /etc/ckupeye/config.sh
```

And to run it, just `ckupeye`.

Or, if you don't want to pollute your '/' namespace,
provide an argument to the configuration file, as so:

```bash
/path/to/ckupeye /path/to/config.sh
```

For the server, I would recommend having an account for backups (or one account per backup for extra security),
and use a [ssh forced command](https://borgbackup.readthedocs.io/en/stable/usage/serve.html#examples).
Add something like the following to the user's `~/.ssh/authorized_keys`:

```
restrict,command="borg serve --restrict-to-repository=/var/lib/borg/clientname --append-only --storage-quota=150G" ssh-ed25519 ... ckupeye@clientname
```

## Configuration

At minimum you will need to set `BORG_REPO` and a `BORG_PASSPHRASE` in the config file.

## Notation

The machine is backed up is called the client,
the machine that stores the backup is called the server.
