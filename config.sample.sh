#!/usr/bin/env bash

BASE_DIR="/var/lib/ckupeye"
BORG_REPO="ssh://user@server:port/~/client"
BORG_PASSPHRASE=""

# Directories to backup
BACKUP_FILES=("/") # What to include in the backup
BACKUP_EXCLUDES=("/var/cache" "**.lock" "**/.cache/") # What to exclue from the backup
BACKUP_TAG="$(date -Isec)" # “Name” of the backup
BACKUP_OPTIONS="--one-file-system --compression auto,zstd" # Configuration of the backup

# Default folder where the database / package managers / ACL exported data will be stored.
EXPORT_DIR="$BASE_DIR"

# Things to export the data of
EXPORT_MARIADB="false"
EXPORT_POSTGRESQL="false"
EXPORT_APT="false"
EXPORT_PACMAN="false"
EXPORT_ACL="false"
EXPORT_ETCKEEPER="false"

# Where to put the exported data
EXPORT_MARIADB_FILE="$EXPORT_DIR/mariadb.sql"
EXPORT_POSTGRESQL_FILE="$EXPORT_DIR/postgresql.sql"
EXPORT_APT_FILE="$EXPORT_DIR/apt.list"
EXPORT_PACMAN_FILE="$EXPORT_DIR/pacman.list"
EXPORT_ACL_FILE="$EXPORT_DIR/acl.list"

# Options for exporting data
EXPORT_ACL_DIRECTORIES=("${BACKUP_FILES[@]}")
EXPORT_ETCKEEPER_MESSAGE="ckupeye backup for $(date -Isec)"
EXPORT_POSTGRESQL_OPTIONS=""
EXPORT_MARIADB_OPTIONS="--all-databases --flush-privileges"
EXPORT_MARIADB_CREDENTIALS="$BASE_DIR/mariadb.cnf"

# Pruning the backup
PRUNE="false"
PRUNE_OPTIONS="--keep-hourly 24 --keep-daily 7 --keep-weekly 4 --keep-monthly 12 --keep-yearly 10"

# Borg specific
BORG_BASE_DIR="$BASE_DIR"
BORG_EXECUTABLE="$(command -v borg)" # Path to BorgBackup executable
BORG_SSH_IDENTITY_FILE="/etc/ckupeye/id_ed25519" # Identity file to use to connect to remote repo (will overwrite $BORG_RSH)
BORG_RSH_EXTRA="" # Options to add to $BORG_RSH (even after overriden by BORG_SSH_IDENTITY_FILE)
BORG_OPTIONS="" # Additional options to pass to BorgBackup
BORG_ENCRYPTION="repokey-blake2" # Passed `borg init`

# Answer borg questions
BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK="no"
BORG_RELOCATED_REPO_ACCESS_IS_OK="no"
BORG_CHECK_I_KNOW_WHAT_I_AM_DOING="NO"
BORG_DELETE_I_KNOW_WHAT_I_AM_DOING="NO"
BORG_RECREATE_I_KNOW_WHAT_I_AM_DOING="NO"

# Ckupeye specific
CLIENT_RUNFILE="$BASE_DIR/ckupeye.pid"

